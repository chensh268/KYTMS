package com.kytms.transportorder.service.Impl;

        import com.kytms.core.entity.OrderReceivingParty;
        import com.kytms.core.service.impl.BaseServiceImpl;
        import com.kytms.transportorder.dao.OrderRPDao;
        import com.kytms.transportorder.service.OrderRPService;
        import org.apache.log4j.Logger;
        import org.springframework.stereotype.Service;

        import javax.annotation.Resource;

/**
 * 辽宁捷畅物流有限公司 -信息技术中心
 * 臧英明
 *
 * @author
 * @create 2017-11-23
 */
@Service(value = "OrderRPService")
public class OrderRPServiceImpl extends BaseServiceImpl<OrderReceivingParty> implements OrderRPService<OrderReceivingParty> {
    private final Logger log = Logger.getLogger(OrderRPServiceImpl.class);//输出Log日志
    private OrderRPDao orderRPDao;

    @Resource(name = "OrderRPDao")
    public void setOrderRPDao(OrderRPDao orderRPDao) {
        super.setBaseDao(orderRPDao);
        this.orderRPDao = orderRPDao;
    }
}
