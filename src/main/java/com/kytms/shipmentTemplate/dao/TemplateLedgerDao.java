package com.kytms.shipmentTemplate.dao;

import com.kytms.core.dao.BaseDao;

public interface TemplateLedgerDao<TemplateLedger>  extends BaseDao<TemplateLedger> {
}
